Identification of an enhancer that increases miR-200b~200a~429 gene expression in breast cancer cells
-----------------------------------------------------------------------------------------------------

Bash and Python scripts to produce data / graphs used in the paper:

<http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0075517>

REQUIRED SOFTWARE
-----------------

 - TMAP - https://github.com/iontorrent/TMAP
 - bedtools - https://code.google.com/p/bedtools
 - Samtools

INSTALLATION
------------

    git clone https://bitbucket.org/sacgf/attema_2013_200_enhancer.git
    cd attema_2013_200_enhancer
    # Install python libraries
    sudo pip install -r requirements.txt

Then edit the file scripts/config.sh, and set the variable:

TMAP_REFERENCE=

To the path to the genome.fa file in the TMAP index directory 

RUNNING
-------

    ./scripts/attema_2013_200_enhancer.sh

 - Separates fastq reads by RACE primers
 - Maps to HG19 with TMAP
 - Generates graphs, stats, bedgraph coverage files

