import HTSeq
import os
import numpy as np

def remove_gz_if_exists(filename):
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
    return filename

def mid_point(iv):
    '''Returns genomic position'''
    g_pos = iv.start_as_pos
    g_pos.pos += int(iv.length / 2)
    return g_pos

def get_sequence_extension(file_name):
    (name, extension) = os.path.splitext(file_name)
    if extension == ".gz":
        extension = os.path.splitext(name)[1] + extension
    return extension

def get_sequence_name(filename):
    filename = remove_gz_if_exists(filename)
    return os.path.splitext(os.path.basename(filename))[0]

def bam_iv_iterator(bam_file):
    for aln in HTSeq.BAM_Reader(bam_file):
        if aln.aligned:
            yield aln.iv

def sequence_file_format(filename):
    filename = remove_gz_if_exists(filename)
        
    if [f for f in [".fa", ".fasta"] if filename.endswith(f)]:
        return "fasta"
    elif [f for f in [".fq", ".fastq", ".txt"] if filename.endswith(f)]:
        return "fastq"
    raise Exception("Unknown sequence file type of %s" % (filename, ))

def calculate_read_depth_in_iv(subject_iv, bam, **kwargs):
    '''
    Calculates read depth over region.
    bam is HTSeq.BAM_Reader() instance - is read as unstranded.
    Position 0 of numpy array is the upstream-most point of subject_iv.
    Returns numpy array of read depth at each basepair in iv
    Set kwarg midpoint=True if you want to count depth over only the midpoint of the read alignment, rather than whole iv'''
    
    use_midpoint = kwargs.get('midpoint', False) #True = uses only midpoint of each alignment, not whole iv
    
    #calculate depth
    genomic_depth_array = HTSeq.GenomicArray("auto", stranded=False, typecode='i')
    
    if use_midpoint:
        for aln in bam[subject_iv]: #only get alignments in this interval
            midpoint_pos = mid_point(aln.iv)
            
            genomic_depth_array[midpoint_pos] += 1
    else:
        for aln in bam[subject_iv]: #only get alignments in this interval
            genomic_depth_array[aln.iv] += 1
            
    #convert to generic array format
    numpy_depth_array = np.zeros(subject_iv.length, dtype='i')
    
    for (iv, depth) in genomic_depth_array[subject_iv].steps():
                
        if subject_iv.strand == "+":
            numpy_start_pos = iv.start - subject_iv.start
            numpy_end_pos = numpy_start_pos + iv.length

        elif subject_iv.strand == '-':
            numpy_start_pos = subject_iv.end - iv.end
            numpy_end_pos = numpy_start_pos + iv.length

        else:
            raise ValueError('Strand should be + or -\n')
            
        numpy_depth_array[numpy_start_pos : numpy_end_pos] = depth #Move the interval to the start of the array.
        
    return numpy_depth_array

def calculate_normalised_read_depth(subject_iv, control_bam, num_control_reads_mapped, expt_bam, num_expt_reads_mapped):
    '''
    control_bam = list of HTSeq alignments (e.g. HTSeq.from BAM_Reader())
    num_control_reads_mapped = total number of reads mapped over whole genome
    Returns numpy array of depth for expt bam, normalised per millions of reads mapped and by depth in control sample.'''
    mark_array = calculate_read_depth_in_iv(subject_iv, expt_bam)
    control_array = calculate_read_depth_in_iv(subject_iv, control_bam)
    
    #normalise per sample per millions of reads mapped
    n_mark_array = mark_array / (num_expt_reads_mapped / 1000000.0)
    n_control_array = control_array / (num_control_reads_mapped / 1000000.0)
    
    #normalise expt against control
    return n_mark_array - n_control_array
