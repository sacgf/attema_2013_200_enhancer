'''
Manage filenames that conform to the pattern CELLTYPE.MARK.LIBRARYPREP.SAMPLEID

Created on May 27, 2012

@author: dlawrence
'''
from collections import defaultdict
import os

class Sample(object):
    '''
    Represents one sequenced ChIP-seq sample
    file_name = bam file of aligned ChIP-seq reads
    reference = reference genome for alignment
    Gets cell type, mark and sample_id attributes from file name
    '''
    def __init__(self, file_name, reference=None):
        self.file_name = file_name
        self.reference = reference
        
        sample_attributes = os.path.basename(file_name).split('.')
        (self.cell_type, self.mark, self.data_type, self.sample_id) = sample_attributes[:4]
        
    def __str__(self):
        return "cell_type: %s mark: %s data_type: %s sample_id: %s" % (self.cell_type, self.mark, self.data_type, self.sample_id)        

class SampleManager(object):
    '''
    Creates a set of Sample instances from sample file names and manages them
    '''
    def __init__(self, file_names_list, reference=None):
        self.samples_by_cell_type = defaultdict(list)
        self.samples_by_mark = defaultdict(list)
        
        assert len(file_names_list) > 0
        for file_name in file_names_list:
            sample = Sample(file_name, reference)
            
            self.samples_by_cell_type[sample.cell_type].append(sample)
            self.samples_by_mark[sample.mark].append(sample)

                
    def __iter__( self ):
        for cell_type_samples in self.samples_by_cell_type.itervalues():
            for sample in cell_type_samples:
                yield sample

    def by_cell_types(self):
        '''Returns list of tuples: (cell type, list of Sample instances)'''
        return self.samples_by_cell_type.iteritems()

    def by_marks(self):
        '''Returns list of tuples: (Mark, list of Sample instances)'''
        return self.samples_by_mark.iteritems()

    def get_cell_types(self):
        ''' Returns a sorted list of cell types '''
        return sorted(self.samples_by_cell_type.keys())
    
    def get_marks(self):
        ''' Returns a sorted list of marks '''
        marks = set()
        for sample_list in self.samples_by_cell_type.itervalues():
            marks.update([s.mark for s in sample_list])
        return sorted(list(marks))
