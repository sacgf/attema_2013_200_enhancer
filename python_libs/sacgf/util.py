import re
import os
import csv

class Struct:
    def __init__(self, **entries): self.__dict__.update(entries)

def file_or_file_name(f, mode=None):
    if isinstance(f, basestring): # Works on unicode
        if 'w' in mode: # Create path if writing
            mk_path_for_file(f)
            
        return open(f, mode)
    elif isinstance(f, file):
        return f # Already a File object
    else:
        raise ValueError("'%s' (%s) not a file or string" % (f, type(f)))

def get_open_func(file_name):
    if file_name.endswith(".gz"):
        return gzip.open
    else:
        return open
    
def mk_path(path):
    if path and not os.path.exists(path):
        os.makedirs(path)

def mk_path_for_file(f):
    mk_path(os.path.dirname(f))

def name_from_file_name(file_name):
    '''Gets file name without extension or directory'''
    return os.path.splitext(os.path.basename(file_name))[0]

def file_to_hash(f, sep=None):
    data = {}
    for line in file_or_file_name(f):
        line = line.rstrip()
        key = line
        value = None
        if sep:
            items = line.split(sep)
            if len(items) >= 2:
                (key, value) = items[0:2]
        data[key] = value
    return data

def line_count(file_name):
    lines = 0
    with open(file_name, "r") as f:
        for _ in f:
            lines += 1
    return lines

def generate_mis_matches(sequence, num_mis_matches):
    mismatches = set()

    if num_mis_matches > 0:
        for i in range(0, len(sequence)):
            mm_list = list(sequence)
            mm_list[i] = '.'
            new_str = ''.join(mm_list)
            mismatches.update(generate_mis_matches(new_str, num_mis_matches-1))
    else:
        mismatches.add(sequence)
    
    return mismatches

def match_patterns_in_file(source, patterns, mandatory):
    data = {}
    i = 0
    for line in source:
        i += 1
        for (name, pattern) in patterns.iteritems():
            match_obj = re.search(pattern, line)
            if match_obj:
                data[name] = match_obj.group(1)

    if mandatory:
        num_lines = i
        for (name, pattern) in patterns.iteritems():
            if not data.get(name):
                error_str = "Pattern: %s = %s had no matches (searched %d lines)" % (name, pattern, num_lines)
                raise ValueError(error_str)

    return data

def write_csv_dict(csv_file, headers, rows, extrasaction=None, dialect=None):
    '''
    default dialect = 'excel', other dialect option: 'excel-tab'
    These are the same optional arguments as csv.DictWriter
    headers=keys for dicts
    rows=list of dicts
    '''    
    if extrasaction is None:
        extrasaction = "raise"
    if dialect is None:
        dialect = 'excel'
    
    f = file_or_file_name(csv_file, "wb")

    writer = csv.DictWriter(f, headers, extrasaction=extrasaction, dialect=dialect)
#        writer.writeheader()
#        e_r_s_a uses python 2.6
    writer.writerow(dict(zip(headers, headers)))
    writer.writerows(rows)
