from matplotlib import pyplot
from sacgf.util import mk_path_for_file
import numpy as np


def stacked_bar_graph(largs, arrays, labels, colors):
    bottom = np.zeros(len(largs), dtype='i')
    for array, label, color in zip(arrays, labels, colors):
        lines = pyplot.bar(largs, array, label=label, color=color, bottom=bottom, linewidth=0)
        bottom += array
    
    return lines

def write_stacked_bar_graph(graph_image, largs, arrays, labels, colors, **kwargs):
    '''largs = x-values
    arrays = list of lists y-values
    labels = list of labels, same length as arrays
    colors = list of colors, same length as arrays
    '''

    title = kwargs.get("title")
    extra_func = kwargs.get("extra_func")
    subtitle = kwargs.get("subtitle")
    axvspan = kwargs.get("axvspan")
    loc = kwargs.get("loc")
    x_label = kwargs.get("x_label")
    y_label = kwargs.get("y_label")
    legend_title = kwargs.get("legend_title")
    legend_props = kwargs.get("legend_props", {})

    fig = pyplot.figure(figsize=(8, 6))
    lines = stacked_bar_graph(largs, arrays, labels, colors)

    if title:
        pyplot.suptitle(title, fontsize=18)
    if subtitle:
        pyplot.title(subtitle, fontsize=12)
    if extra_func:
        extra_func(pyplot, fig, lines)
    if axvspan:
        pyplot.axvspan(axvspan.xmin, axvspan.xmax, facecolor=axvspan.color, alpha=axvspan.alpha)
    if x_label:
        pyplot.xlabel(x_label)
    if y_label:
        pyplot.ylabel(y_label)

    pyplot.xlim(xmin=largs[0], xmax=largs[-1]+1)
    pyplot.legend(loc=loc, title=legend_title, prop=legend_props)
    
    mk_path_for_file(graph_image)
    pyplot.savefig(graph_image, dpi=300)
    pyplot.close()
