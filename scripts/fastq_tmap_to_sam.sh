#!/bin/bash

. $(dirname $0)/config.sh
load_module tmap

FASTQ=${1}

if [ -z ${FASTQ} ]; then
        echo "No fastq file provided as argument"
        exit;
elif [ ! -e ${FASTQ} ]; then
        echo "No fastq file ('${FASTQ}') found"
        exit
else
        echo "FASTQ = '${FASTQ}'";
fi

NAME=$(get_name ${FASTQ}).tmap.${BUILD}
SAM=${NAME}.sam

tmap mapall -f ${TMAP_REFERENCE} -r ${FASTQ} -n ${CORES} -v stage1 map1 map2 map3 > ${SAM}
