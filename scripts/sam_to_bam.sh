#!/bin/bash

. $(dirname $0)/config.sh
load_module samtools

set -e

SAM=$1
if [ -z ${SAM} ]; then
        echo "No argument provided"
        exit 1;
fi
NAME=$(get_name ${SAM})

samtools view -b -S ${SAM} > ${NAME}.unsorted.bam
samtools sort ${NAME}.unsorted.bam ${NAME}
rm ${NAME}.unsorted.bam

BAM=${NAME}.bam
samtools index ${BAM}

mkdir -p flagstats
BAM_FLAGSTAT=flagstats/$(get_name ${BAM}).flagstat.txt
samtools flagstat ${BAM} > ${BAM_FLAGSTAT}
