# Local machine settings

# Set variable to wherever you have installed the TMAP index. For instance mine is:
#TMAP_REFERENCE=/data/sacgf/reference/iGenomes/Homo_sapiens/UCSC/hg19/Sequence/TMAPIndex/genome.fa
TMAP_REFERENCE=

# How many cores to run TMAP with
CORES=4

# Uncomment this variable (with local settings) to load appropriate PBS modules (eg on a cluster)
# Leave it commented out to just try and run "samtools" etc on the commandline (using paths)
#export BASH_MODULE_INIT=/opt/shared/system/Modules/3.2.7/init/bash

# Global settings

ORGANISM=Homo_sapiens
BUILD=hg19

###############################
# Util functions

function load_module {
	local MODULE=$1
	if [ ! -z ${BASH_MODULE_INIT} ]; then
		if [ -z ${MODULE_VERSION_STACK} ]; then
			echo "Loading ${BASH_MODULE_INIT}" >&2
			. ${BASH_MODULE_INIT}
		fi
		echo "module load ${MODULE}" >&2
		module load ${MODULE}
	else
		echo "Didn't call module, I hope ${MODULE} is in your path!" 1>&2
	fi
}

function get_name {
        if [[ $# -ne 1 || -z $1 ]]; then
                echo "get_name must be passed exactly 1 argument" 1>&2
		exit 1
        fi
        local FILENAME=$(basename $1)
        local EXTENSION=${FILENAME##*.}
        local NAME="${FILENAME%.*}"
        if [ ${EXTENSION} == "gz" ]; then # get next one...
                NAME=$(get_name ${NAME})
        fi
        echo ${NAME}
}

function get_extension {
        if [[ $# -ne 1 || -z $1 ]]; then
                echo "get_extension must be passed exactly 1 argument" 1>&2
		exit 1
        fi
        local FILENAME=${1}
        local EXTENSION=${FILENAME##*.}
        local NAME="${FILENAME%.*}"
        if [ ${EXTENSION} == "gz" ]; then # get next one...
                EXTENSION=$(get_extension ${NAME}).${EXTENSION}
        fi
        echo ${EXTENSION}
}
