#!/usr/bin/env python

'''
Created on Oct 15, 2012

TODO: Put lots of things (eg GSP3_RACE etc) in config files...

@author: dlawrence
'''

import matplotlib # Import line 1
matplotlib.use('agg', warn=False) # Import line 2: Has to be before other matplotlib ops in imports
from collections import defaultdict
from matplotlib.ticker import Formatter, MaxNLocator
from sacgf import genomics, graphing, sample_manager, util
import HTSeq
import glob
import numpy as np
import os
import pandas as pd


GENOME_BUILD = "hg19"
GSP3_RACE = HTSeq.GenomicInterval("chr1", 1093129, 1093149, '.')
GSP9_RACE = HTSeq.GenomicInterval("chr1", 1093191, 1093211, '.')
BAM_DIR = "output/aligned"

def match_identifier(string, identifiers):
    for i in identifiers:
        if string.find(i) != -1:
            return i

    raise ValueError("%s had no match for %s" % (string, identifiers))

def reversed_xaxis_labels(pyplot, fig, lines):
    (locs, labels) = pyplot.xticks()
    new_labels = [tick_label.get_text() for tick_label in reversed(labels)]
    print "old Labels = %s, New labels = %s" % (labels, new_labels)
    pyplot.xticks(locs, new_labels)

class LabelFormatter(Formatter):
    def __init__(self, label_spacings, reverse, axis_length):
        self.label_spacings = label_spacings
        self.reverse = reverse
        self.axis_length = axis_length
        
    def __call__(self, x, pos=None):
        if self.reverse:
            x = self.axis_length - x
        x = int(x)
        if x % self.label_spacings == 0:
            return "%d" % x
        else:
            return ""

def main():
    bam_files = glob.glob(os.path.join(BAM_DIR, "*.bam"))
    samples = sample_manager.SampleManager(bam_files)
    graph_dir = os.path.join("output", "graphs")

    columns = {}
    for (cell_type, sample_list) in samples.by_cell_types():
        #print "CellType = %s, samples = %s" % (cell_type, sample_list)

        samples_by_end = defaultdict(list)
        for sample in sample_list:
            identifiers = ["5P", "3P"]
            end_id = match_identifier(sample.mark, identifiers)
            samples_by_end[end_id].append(sample)

        for (end_id, grouped_samples) in samples_by_end.iteritems():

            start_func = lambda iv : iv.start
            end_of_fragment_func = lambda iv : iv.end

            if end_id == "5P":
                primer = GSP3_RACE
                color = "green"
                (start_func, end_of_fragment_func) = (end_of_fragment_func, start_func)
                graph_range = primer.copy()
                graph_range.start -= 250
                loc = "upper left"
                reverse = True
            elif end_id == "3P":
                primer = GSP9_RACE
                color = "magenta"
                graph_range = primer.copy()
                graph_range.end += 250
                loc = "upper right"
                reverse = False
            else:
                raise ValueError("Unknown end_id of %s" % end_id)

            num_minor_ticks = graph_range.length
            num_major_ticks = graph_range.length / 10
            def extra_func(pyplot, fig, lines):
                ax = pyplot.gca()
                ax.xaxis.set_major_locator(MaxNLocator(num_major_ticks))
                ax.xaxis.set_minor_locator(MaxNLocator(num_minor_ticks))
                ax.xaxis.set_major_formatter(LabelFormatter(20, reverse, graph_range.length))

            #print "end_id = %s, length = %d" % (end_id, graph_range.length)

            primer_vis = util.Struct(color=color, alpha=0.7, xmin=primer.start - graph_range.start, xmax=primer.end - graph_range.start)

            sum_reads_in = 0
            sum_reads_total = 0
            arrays = []
            labels = []
            for sample in sorted(grouped_samples, key=lambda sample : sample.mark):
                labels.append(sample.mark)
                iterator = genomics.bam_iv_iterator(sample.file_name)
                (array, reads_in, reads_total) = histogram_array(graph_range, iterator, end_of_fragment_func)
                arrays.append(array)

                sum_reads_in += reads_in
                sum_reads_total += reads_total

            largs = np.arange( graph_range.length )
            title = "%s %s" % (cell_type, end_id)
#            reads_percent = 100.0 * sum_reads_in / sum_reads_total
            subtitle="primer: %s (%s)" % (primer, GENOME_BUILD)  #. Graph contains %.2f%% of reads" % (primer, reads_percent)
            x_label = "Distance from primer"

            # Issue immun-138: end depths
            depth = cumulative_depth_from_extending_starts(end_id, arrays)
            name = "%s_%s" % (cell_type, end_id)
            columns[name] = depth
            
            def extra_func2(pyplot, fig, lines):
                # TODO: rewrite to not be such a hack
                pyplot.plot(depth, label="cumulative depth")
                extra_func(pyplot, fig, lines)

            graph_types = {"no_depth" : extra_func,
                           "depth" : extra_func2}
            for (description, func) in graph_types.iteritems():
                graph_image = os.path.join(graph_dir, "_".join([cell_type, end_id, description]) + ".png")
                graphing.write_stacked_bar_graph(graph_image, largs, arrays, labels, ["red", "blue"], title=title, subtitle=subtitle, axvspan=primer_vis, extra_func=func, loc=loc, x_label=x_label)

    csv_filename = os.path.join(graph_dir, "RACEseq_depths.csv")
    df = pd.DataFrame(columns)
    df.to_csv(csv_filename)


def cumulative_depth_from_extending_starts(end_id, arrays):
    depth = np.zeros(len(arrays[0]), dtype='i')
    for array in arrays:
        for (i, num_starts) in enumerate(array):
            if end_id == '5P':
                depth[i:] += num_starts
            else:
                depth[:i+1] += num_starts
    return depth

def histogram_array(graph_range, iterator, end_of_fragment_func):
    array = np.zeros( graph_range.length, dtype='i' )
    
    reads_total = 0
    reads_in_range = 0
    
    for iv in iterator:
        reads_total += 1
        if graph_range.contains(iv):
            i = end_of_fragment_func(iv) - graph_range.start
            #print "iv %s pos = %d" % (iv, i)
            array[i] += 1
            reads_in_range += 1
#        else:
#            print "%s not in %s" % (iv, graph_range)

    return (array, reads_in_range, reads_total)            

if __name__ == '__main__':
    main()
