#!/bin/bash

SCRIPTS_DIR=$(readlink -nf $(dirname $0))
. ${SCRIPTS_DIR}/config.sh

FASTQ=${1}
CONFIG_DIR=${2}

set -e

if [[ -z ${FASTQ} || ! -d ${CONFIG_DIR} ]]; then
	echo "Usage $(basename ${0}): fastq config_dir"
	exit 1;
fi


NAME=$(get_name ${FASTQ})
EXTENSION=$(get_extension ${FASTQ})

ALIGNED_DIR=output/aligned
mkdir -p ${ALIGNED_DIR}

for cfg in ${CONFIG_DIR}/*RACE.txt; do
	RACE_PRODUCT=$(get_name ${cfg})
	echo "raceseq trim ${RACE_PRODUCT}"

	${SCRIPTS_DIR}/raceseq_trim_pipeline.py --step-file=${cfg} ${FASTQ}

	# Align the files
	TRIMMED_FASTQ_NAME=${NAME}.${RACE_PRODUCT}.trimmed

	cd output/aligned
	MAPPED_NAME=${TRIMMED_FASTQ_NAME}.tmap.hg19
	SAM=${MAPPED_NAME}.sam
	echo "Aligning ${MAPPED_NAME}"
	${SCRIPTS_DIR}/fastq_tmap_to_sam.sh ../unaligned/${TRIMMED_FASTQ_NAME}.${EXTENSION}
	${SCRIPTS_DIR}/sam_to_bam.sh ${SAM}
	rm ${SAM}
	cd ../..
done


