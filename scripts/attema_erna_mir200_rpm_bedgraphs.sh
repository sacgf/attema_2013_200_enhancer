#!/bin/bash

MIR_200_REGION=chr1:1090000-1105000

MIR_BAM_OUTPUT_DIR=output/aligned/mir_bams
BEDGRAPH_OUTPUT_DIR=output/bedgraphs

mkdir -p ${MIR_BAM_OUTPUT_DIR}
mkdir -p ${BEDGRAPH_OUTPUT_DIR}


for BAM in ${@}; do
	MIR_BAM=${MIR_BAM_OUTPUT_DIR}/$(basename ${BAM} .bam).mir200locus.bam
	samtools view -b ${BAM} $MIR_200_REGION > ${MIR_BAM}
        
	FLAGSTATS=$(dirname ${BAM})/flagstats/$(basename ${BAM} .bam).flagstat.txt
	if [ ! -e ${FLAGSTATS} ]; then
		echo "Cannot open file: '${FLAGSTATS}'" >&2
		exit 1;
	fi
	
	READS=$(grep "in total" ${FLAGSTATS} | cut -f 1 -d' ')
	SCALE_FACTOR=$(echo ${READS} / 1000000.0 | bc -l)
	echo "Scale factor for " $BAM " is " $SCALE_FACTOR
	BEDGRAPH=${BEDGRAPH_OUTPUT_DIR}/$(basename ${BAM} .bam).rpm.bedgraph
	genomeCoverageBed -ibam $MIR_BAM -bg -scale ${SCALE_FACTOR} > $BEDGRAPH
done

