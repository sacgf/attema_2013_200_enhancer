#!/bin/bash

set -e

SCRIPTS_DIR=$(dirname $0)
. ${SCRIPTS_DIR}/config.sh

if [[ -z ${TMAP_REFERENCE} ]]; then
    echo "You need to set the TMAP_REFERENCE variable in config.sh" >&2
    exit 1;
elif [[ ! -e ${TMAP_REFERENCE} ]]; then
    echo "Cannot access file: '$TMAP_REFERENCE'" >&2
    exit 1;
fi

BASE_DIR=${SCRIPTS_DIR}/..
ERNA_DATA_DIR=${BASE_DIR}/data/mir200_eRNA_Ion_Torrent
FASTQ=${ERNA_DATA_DIR}/R_2012_09_08_13_58_17_user_SN1-59_Auto_SN1-59_60.fastq
BARCODES=${ERNA_DATA_DIR}/barcodes/barcodes.R_2012_09_08_13_58_17_user_SN1-59_Auto_SN1-59_60.txt
RACESEQ_TRIMMING_CONFIG_DIR=${ERNA_DATA_DIR}/RACE_primers
SPLIT_FASTQ_DIR=output/barcode_split_fastq
BAMS_DIR=output/aligned

PYTHONLIBS_DIR=${BASE_DIR}/python_libs
export PYTHONPATH=${PYTHONLIBS_DIR}:${PYTHONPATH}

echo "Splitting barcodes"
mkdir -p ${SPLIT_FASTQ_DIR}
${SCRIPTS_DIR}/fastx_split_barcodes_and_trim.py --bcfile ${BARCODES} --mismatches=0 --bcfile ${BARCODES} --mismatches=1 --prefix=${SPLIT_FASTQ_DIR}/ ${FASTQ}
echo "Deleting unmatched file"
rm ${SPLIT_FASTQ_DIR}/unmatched*

for fastq in ${SPLIT_FASTQ_DIR}/*; do
	echo "RACEseq for ${fastq}"
	${SCRIPTS_DIR}/raceseq_pipeline.sh ${fastq} ${RACESEQ_TRIMMING_CONFIG_DIR} # &
done

wait

${SCRIPTS_DIR}/attema_erna_mir200_rpm_bedgraphs.sh ${BAMS_DIR}/*.bam

${SCRIPTS_DIR}/raceseq_graph.py

