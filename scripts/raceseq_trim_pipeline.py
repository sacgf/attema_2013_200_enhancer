#!/usr/bin/env python
'''
Sep 21, 2012 - dlawrence - Initial version
Oct 19, 2012 - dlawrence - Use text based configuration

@author: dlawrence
'''

from argparse import ArgumentParser
import os
import re
import shutil
from sacgf import util

def fastq_reads(fastq):
    return util.line_count(fastq) / 4

def trim_op(cell_type, fastq, op):
    args = []

    end_argument = {"5'" : "front", "3'" : "adapter"}
    args.append('--%s="%s"' % (end_argument[op.end], op.sequence))

    # Default is to trim
    if not op.trim:
        args.append("--no-trim")

    if op.discard:
        untrimmed_fastq = os.path.join("output", "scratch", "discarded_reads", "%s_%s_untrimmed.fastq" % (cell_type, op.id))
        util.mk_path_for_file(untrimmed_fastq)
        args.append('--untrimmed-output="%s"' % untrimmed_fastq)

    out_fastq = os.path.join("output", "scratch", "%s_%s.fastq" % (cell_type, op.id))

    util.mk_path_for_file(out_fastq)

    cmd = 'cutadapt %s --minimum-length=%d --output="%s" %s' % (" ".join(args), op.minimum_length, out_fastq, fastq)
    #print "cmd = %s" % cmd
    (_, child_stdout) = os.popen2(cmd)

    patterns = {
        "in"     : "Processed reads:\s+(\d+)",
        "trimmed"   : "(Trimmed|Matched) reads:\s+(\d+)",
        "short"     : "Too short reads:\s+(\d+)",
    }
    stats = util.match_patterns_in_file(child_stdout, patterns, True)
    stats.update({"step"     : op.step,
                  "op"       : op.operation,
                  "end"      : op.end,
                  "sequence" : op.sequence,
                  "trim"     : op.trim,
                  "discard"  : op.discard,
                  "out"      : fastq_reads(out_fastq),})
    
    return (out_fastq, stats)

def load_pipeline_steps(file_name, minimum_length):
    name = util.name_from_file_name(file_name)
    pipeline = []
    with open(file_name) as f:
        for (i, line) in enumerate(f):
            if line.startswith('#'):
                continue
            line = line.rstrip()
            step = i+1
            op_id = "%s_op_%d" % (name, step)

            columns = re.split("\s+", line)
            if len(columns) != 3:
                raise ValueError("'%s' did not contain 3 whitespace separated values" % line)
            operation = columns[0].upper()
            trim = operation.find("TRIM") != -1
            discard = operation.find("MATCH") != -1
            end = columns[1]
            sequence = columns[2]

            op = util.Struct(id=op_id, step=step, operation=operation, sequence=sequence, end=end, trim=trim, discard=discard, minimum_length=minimum_length)
            pipeline.append(op)

    return pipeline
    

def main(args):
    fastq = args.file
    if not os.path.exists(fastq):
        raise IOError("'%s' does not exist" % fastq)
    pipeline_id = util.name_from_file_name(args.step_file)
    pipeline = load_pipeline_steps(args.step_file, args.minimum_length)

    rows = []
    sample = util.name_from_file_name(fastq)

    for op in pipeline:
        (out_fastq, stats) = trim_op(sample, fastq, op)
        rows.append(stats)

        dst = os.path.join("output", "unaligned", "%s.%s.trimmed.fastq" % (sample, pipeline_id))
        util.mk_path_for_file(dst)
        shutil.copy(out_fastq, dst)
        fastq = out_fastq

    csv_file_name = os.path.join("output", "%s_%s_stats.csv" % (sample, pipeline_id))
    util.write_csv_dict(csv_file_name, ["step", "op", "end", "sequence", "trim", "discard", "in", "trimmed", "short", "out"], rows)
    

def handle_args():
    # Args
    parser = ArgumentParser(description='Trimming Pipeline')
    parser.add_argument('--minimum-length', default=15,         help="Discard sequences shorter than this (15)")
    parser.add_argument('--step-file',                          help="Pipeline steps text file")
    parser.add_argument('file',             metavar='file',     help='Fasta/Fastq file')
    return parser.parse_args()

if __name__ == '__main__':
    args = handle_args()
    main(args)
